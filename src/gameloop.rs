use std::thread;
use std::thread::JoinHandle;
use crate::chip8::ChipEight;
use std::time::Duration;

use crossbeam_channel::{Sender, unbounded};
use crate::gameloop::GameLoopState::{RUNNING, PAUSED, INIT};
use crate::gameloop::GameLoopMessage::{Start, Draw, Stop, Pause, KeyPress, KeyRelease};
use std::sync::{Mutex, Arc};
use std::borrow::{Borrow, BorrowMut};
use std::ops::DerefMut;
use crate::gameloop::GameEvent::UpdateDraw;
use log::{info, debug, trace, warn};
use std::fmt::{Display, Formatter, Error};

enum GameLoopState {
    INIT,
    RUNNING,
    PAUSED
}

impl Display for GameLoopState {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        let state_str = match self {
            INIT => "INIT",
            RUNNING => "RUNNING",
            PAUSED => "PAUSED"
        };
       write!(f, "{}", state_str)
    }
}

enum GameLoopMessage {
    Start,
    Pause,
    Stop,
    KeyPress(usize),
    KeyRelease(usize),

    Draw(Vec<u8>),
}

pub struct GameLoop {
    game_name: String,
    game_bytes: Vec<u8>,
    chip8_thread: Option<JoinHandle<()>>,
    chip8_sender: Sender<GameLoopMessage>,
}

pub enum GameEvent {
    UpdateDraw(Vec<u8>)
}

impl GameLoop {

    pub fn init(game_name: String, game_bytes: Vec<u8>, game_event_sender: Sender<GameEvent>) -> GameLoop {
        info!("Initializing Game: {}", game_name);
        let mut chip8 = ChipEight::init();
        chip8.load_game(&game_bytes);

        let (s, r) = unbounded();
        let mut game_loop = GameLoop {
            game_name,
            game_bytes,
            chip8_thread: None,
            chip8_sender: s,
        };

        let thread_receiver= r.clone();
        game_loop.chip8_thread = Some(thread::spawn(move || {

            let mut state = INIT;

            loop {
                match state {
                    RUNNING => {
                        chip8.cycle();

                        if chip8.should_draw {
                            // notify draw listener
                            chip8.should_draw = false;
                            game_event_sender.send(UpdateDraw(chip8.get_gfx())).unwrap();
                        }

                        thread::sleep(Duration::from_millis(2)); // roughly 60hz
                    },
                    _ => {}
                }

               for msg in thread_receiver.try_iter() {
                   match msg {
                       GameLoopMessage::Start => {
                           debug!("{} => {}", state, RUNNING);
                           state = RUNNING
                       },
                       GameLoopMessage::Pause => {
                           debug!("{} => {}", state, PAUSED);
                           state = PAUSED
                       },
                       GameLoopMessage::Stop => {
                           debug!("{} => STOPPED", state);
                           return;
                       },
                       GameLoopMessage::KeyPress(k) => {
                           chip8.key[k] = 1;
                       },
                       GameLoopMessage::KeyRelease(k) => {
                           chip8.key[k] = 0;
                       }
                       _ => {}
                   }
               }
            }

        }));

        game_loop
    }

    pub fn start(&self) {
        self.chip8_sender.send(Start).unwrap();
    }

    pub fn pause(&self) {
        self.chip8_sender.send(Pause).unwrap();
    }

    pub fn stop(self) {
        self.chip8_sender.send(Stop).unwrap();
        self.chip8_thread.unwrap().join();
    }

    pub fn key_press(&self, value: u32) {
        self.chip8_sender.send(KeyPress(value as usize));
    }

    pub fn key_release(&self, value: u32) {
        self.chip8_sender.send(KeyRelease(value as usize));
    }
}