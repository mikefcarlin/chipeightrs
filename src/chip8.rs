use rand::Rng;
use log::{info, debug, trace, warn, error};
use std::num::Wrapping;
use std::thread::sleep;
use std::time;

const FONT_SET: [u8; 80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80 // F
];

pub struct ChipEight {
    memory: [u8; 4096],
    gfx: [u8; 32 * 64],

    v: [Wrapping<u8>; 16],
    op: u16,
    stack: Vec<u16>,
    pc: u16,
    sp: u8,
    idx: u16,

    pub key: [u8; 16],
    sound_timer: u8,
    delay_time: u8,
    pub should_draw: bool,
}

impl ChipEight {
    pub fn init() -> ChipEight {
        let mut chip8 = ChipEight {
            memory: [0; 4096],
            gfx: [0; 32 * 64],
            v: [Wrapping(0); 16],
            op: 0,
            stack: vec![],
            pc: 0x200,
            idx: 0,
            sp: 0,
            key: [0; 16],
            sound_timer: 0,
            delay_time: 0,
            should_draw: false
        };

        chip8.memory[..80].clone_from_slice(&FONT_SET);

        chip8
    }

    pub fn load_game(&mut self, bytes: &Vec<u8>) {
       for (i, byte) in bytes.iter().enumerate() {
           self.memory[0x200 + i] = byte.clone();
       }
    }

    pub fn get_gfx(&self) -> Vec<u8> {
        self.gfx.to_vec()
    }

    pub fn cycle(&mut self) {

        let opcode = self.fetch_opcode();

        self.decode_and_execute(opcode);

        self.update_timers();
    }



    fn fetch_opcode(&self) -> u16 {
        (self.memory[self.pc as usize] as u16) << 8 | self.memory[(self.pc + 1) as usize] as u16
    }

    fn decode_and_execute(&mut self, opcode: u16) {
        let mut inc_pc = true;

        match opcode & 0xF000 {
            0x0 => {
                match opcode & 0xF {
                    0x0 => { // clear display
                        debug!("0x{:X}: Clear Display", opcode);
                        for i in 0..self.gfx.len() {
                            self.gfx[i] = 0;
                        }
                    },
                    0xE => { // Return from a subroutine
                        debug!("0x{:X}: Return", opcode);
                      self.pc = self.stack.pop().unwrap();
                    },
                    _ => {
                        error!("Unknown opcode: 0x{:X}", opcode);
                    }
                }
            },
            0x1000 => { // Jump to location
                self.pc = opcode & 0xFFF;
                debug!("0x{:X}: Jump to (0x{:X}) ", opcode, self.pc);

                inc_pc = false;
            },
            0x2000 => { // Call Addr
                self.stack.push(self.pc);
                self.pc = opcode & 0xFFF;
                debug!("0x{:X}: Call Addr (0x{:X})", opcode, self.pc);

                inc_pc = false;
            },
            0x3000 => { // Skip next instruction if Vx == kk where 0x3xkk
                let x = ((opcode & 0xF00) >> 8) as usize;
                let kk = (opcode & 0xFF) as u8;

                debug!("0x{:X}: Skip next instruction if Vx({:X}) == kk({:X})", opcode, x, kk);
                if self.v[x as usize].0 == kk {
                    self.pc += 2;
                }
            },
            0x4000 => { // Skip next instruction if Vx != kk where 0x4xkk
                debug!("0x{:X}: Skip next instruction if Vx != kk", opcode);
                let x = ((opcode & 0xF00) >> 8) as usize;
                let kk = (opcode & 0xFF) as u8;

                if self.v[x as usize].0 != kk {
                    self.pc += 2;
                }
            },
            0x5000 => { // Skip next instruction if Vx = Vy where 0x5xy0
                debug!("0x{:X}: Skip next instruction if Vx == Vy", opcode);
                let x = ((opcode & 0xF00) >> 8) as usize;
                let y = ((opcode & 0xF0) >> 4) as usize;

                if self.v[x] == self.v[y] {
                    self.pc += 2;
                }
            },
            0x6000 => {  // Set Vx = kk where 0x6xkk
                let x = ((opcode & 0xF00) >> 8) as usize;
                let kk = (opcode & 0xFF) as u8;

                debug!("0x{:X}: set Vx({:X}) == kk({:X})", opcode, x, kk);
                self.v[x] = Wrapping(kk);
            },
            0x7000 => { // Set Vx = Vx + kk where 0x7xkk
                debug!("0x{:X}: set Vx += kk", opcode);
                let x = ((opcode & 0xF00) >> 8) as usize;
                let kk = (opcode & 0xFF) as u8;

                self.v[x] += Wrapping(kk);
            },
            0x8000 => {
                let x = ((opcode & 0xF00) >> 8) as usize;
                let y = ((opcode & 0xF0) >> 4) as usize;

                match opcode & 0x000F {
                    0x0 => {  // Set Vx = Vy where 0x8xy0
                        debug!("0x{:X}: set Vx = Vy", opcode);
                        self.v[x] = self.v[y]
                    },
                    0x1 => { //  Set Vx = Vx OR Vy where 0x8xy1
                        debug!("0x{:X}: set Vx |= Vy", opcode);
                        self.v[x] |= self.v[y]
                    },
                    0x2 => { //  Set Vx = Vx AND Vy where 0x8xy2
                        debug!("0x{:X}: set Vx &= Vy", opcode);
                        self.v[x] = self.v[x] & self.v[y]
                    },
                    0x3 => { //  Set Vx = Vx XOR Vy where 0x8xy3
                        debug!("0x{:X}: set Vx ^= Vy", opcode);
                        self.v[x] ^= self.v[y]
                    },
                    0x4 => { //  Set Vx = Vx + Vy, set VF = carry where 0x8xy4
                        debug!("0x{:X}: set Vx += Vy, VF = carry", opcode);
                        let carry : u16 = (self.v[x].0 as u16) + (self.v[y].0 as u16);
                        if carry > 255 {
                            self.v[0xF] = Wrapping(1);
                        }
                        self.v[x] = Wrapping((carry & 0xFFFF) as u8);
                    }
                    0x5 => { // Set Vx = Vx - Vy, set VF = NOT borrow.
                        //  If Vx > Vy, then VF is set to 1, otherwise 0. Then Vy is subtracted from Vx, and the results stored in Vx.

                        debug!("0x{:X}: set Vx += Vy, VF = NOT borrow", opcode);
                        if self.v[x] > self.v[y] {
                            self.v[0xF] = Wrapping(1);
                        } else {
                            self.v[0xF] = Wrapping(0);
                        }

                        self.v[x] = self.v[x] - self.v[y];
                    },
                    0x6 => { // Set Vx = Vx SHR 1.
                        //  If the least-significant bit of Vx is 1, then VF is set to 1, otherwise 0. Then Vx is divided by 2.
                        debug!("0x{:X}: set Vx = Vx >> 1", opcode);

                        if (self.v[x].0 & 0x1) == 1 {
                            self.v[0xF] = Wrapping(1);
                        } else {
                            self.v[0xF] = Wrapping(0);
                        }

                        self.v[x] = self.v[x] >> 1;
                    },
                    0x7 => { // Set Vx = Vy - Vx, set VF = NOT borrow.
                        // If Vy > Vx, then VF is set to 1, otherwise 0. Then Vx is subtracted from Vy, and the results stored in Vx.
                        debug!("0x{:X}: set Vx = Vy - Vx, VF = NOT borrow", opcode);
                        if self.v[y] > self.v[x] {
                            self.v[0xF] = Wrapping(1);
                        } else {
                            self.v[0xF] = Wrapping(0);
                        }

                        self.v[x] = self.v[y] - self.v[x];
                    },
                    0xE => { // Set Vx = Vx SHL 1.
                        // If the most-significant bit of Vx is 1, then VF is set to 1, otherwise to 0. Then Vx is multiplied by 2
                        debug!("0x{:X}: set Vx = Vx << 1", opcode);
                        if (self.v[x] >> 0x7).0 == 1 {
                            self.v[0xF] = Wrapping(1);
                        } else {
                            self.v[0xF] = Wrapping(0);
                        }

                        self.v[x] = self.v[x] << 1;
                    }
                    _ => {
                        error!("Unknown opcode: 0x{:X}", opcode);
                    }
                }
            },
            0x9000 => { // Skip next instruction if Vx != Vy.
                // The values of Vx and Vy are compared, and if they are not equal, the program counter is increased by 2.
                let x = ((opcode & 0xF00) >> 8) as usize;
                let y = ((opcode & 0xF0) >> 4) as usize;

                debug!("0x{:X}: SNE if Vx != Vx", opcode);
                if self.v[x] != self.v[y] {
                    self.pc += 2;
                }
            },
            0xA000 => { // Set I = nnn.
                // The value of register I is set to nnn.
                let nnn = opcode & 0xFFF;
                debug!("0x{:X}: Set I = nnn (0x{:X})", opcode, nnn);
                self.idx = nnn;
            },
            0xB000 => { // Jump to location nnn + V0.
                // The program counter is set to nnn plus the value of V0.
                let nnn = opcode & 0xFFF;
                debug!("0x{:X}: Jump to nnn (0x{:X}) + v0", opcode, nnn);
                self.pc = nnn + (self.v[0].0 as u16);
                inc_pc = false;
            },
            0xC000 => { // Set Vx = random byte AND kk.
                // The interpreter generates a random number from 0 to 255, which is then ANDed with the value kk.
                // The results are stored in Vx. See instruction 8xy2 for more information on AND.
                debug!("0x{:X}: set Vx = random byte & kk", opcode);

                let x = ((opcode & 0xF00) >> 8) as usize;
                let kk = (opcode & 0xFF) as u8;
                let rand : u8 = rand::thread_rng().gen();

                self.v[x] = Wrapping(rand & kk);
            },
            0xD000 => { // Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision.
                // The interpreter reads n bytes from memory, starting at the address stored in I.
                // These bytes are then displayed as sprites on screen at coordinates (Vx, Vy).
                // Sprites are XORed onto the existing screen. If this causes any pixels to be erased,
                // VF is set to 1, otherwise it is set to 0. If the sprite is positioned so part of
                // it is outside the coordinates of the display, it wraps around to the opposite side
                // of the screen. See instruction 8xy3 for more information on XOR, and section 2.4,
                // Display, for more information on the Chip-8 screen and sprites.
                let x = self.v[((opcode & 0x0F00) >> 8) as usize].0 as usize;
                let y = self.v[((opcode & 0x00F0) >> 4) as usize].0 as usize;
                let height = (opcode & 0xF) as usize;
                debug!("0x{:X}: Draw", opcode);

                self.v[0xF] = Wrapping(0);
                for yline in 0..height {
                    let pixel = self.memory[(self.idx + (yline as u16)) as usize];
                    for xline in 0..8 {
                        if (pixel & (0x80 >> xline)) != 0 {
                            if self.gfx[x + (xline as usize) + ((y + yline) * 64)] == 1 {
                                self.v[0xF] = Wrapping(1);
                            }
                            self.gfx[x + (xline as usize) + ((y + yline) * 64)] ^= 1;

                        }
                    }
                }

//                println!("x: {}, y: {}", x, y);
//                for (i, g) in self.gfx.iter().enumerate() {
//                    if g == &0_u8 {
//                        print!("\u{00B7}");
//                    } else {
//                        print!("\u{2588}");
//                    }
//                    if i % 64 == 0 {
//                        println!();
//                    }
//                }
//                println!();
                self.should_draw = true;
            },
            0xE000 => {
                let x = ((opcode & 0x0F00) >> 8) as usize;
                match opcode & 0xFF {
                    0x9E => { // Skip next instruction if key with the value of Vx is pressed.
                        // Checks the keyboard, and if the key corresponding to the value of Vx is
                        // currently in the down position, PC is increased by 2.
                        debug!("0x{:X}: Skip next instruction if key Vx pressed", opcode);

                        if self.key[self.v[x].0 as usize] != 0 {
                            self.pc += 2;
                        }
                    },
                    0xA1 => { // Skip next instruction if key with the value of Vx is *not* pressed.
                        // Checks the keyboard, and if the key corresponding to the value of Vx is
                        // currently in the down position, PC is increased by 2.
                        debug!("0x{:X}: Skip next instruction if key Vx NOT pressed", opcode);
                        if self.key[self.v[x].0 as usize] == 0 {
                            self.pc += 2;
                        }
                    },
                    _ => {
                        error!("Unknown opcode: 0x{:X}", opcode);
                    }
                }
            },
            0xF000 => {
                let x = ((opcode & 0xF00) >> 8) as usize;
                match opcode & 0xFF {
                    0x07 => { // Set Vx = delay timer value.
                        // The value of DT is placed into Vx.
                        self.v[x] = Wrapping(self.delay_time);
                        debug!("0x{:X}: Set Vx = delay timer", opcode);
                    },
                    0x0A => { // Wait for a key press, store the value of the key in Vx.
                        // All execution stops until a key is pressed, then the value of that key is stored in Vx.
                        debug!("0x{:X}: Wait for key press, store in Vx", opcode);
                        inc_pc = false;
                        for i in 0..self.key.len() {
                           if self.key[i] != 0 {
                               self.v[x] = Wrapping(self.key[i]);
                               inc_pc = true;
                               break;
                           }
                        }
                    },
                    0x15 => { // Set delay timer = Vx.
                        // DT is set equal to the value of Vx.
                        debug!("0x{:X}: Set delay timer = Vx", opcode);
                        self.delay_time = self.v[x].0;
                    },
                    0x18 => { // Set sound timer = Vx.
                        // ST is set equal to the value of Vx.
                        debug!("0x{:X}: Set sound timer = Vx", opcode);
                        self.sound_timer = self.v[x].0;
                    },
                    0x1E => { // Set I = I + Vx.
                        // The values of I and Vx are added, and the results are stored in I.
                        debug!("0x{:X}: Set idx += Vx", opcode);
                        self.idx = self.idx + (self.v[x].0 as u16);
                    },
                    0x29 => { // Set I = location of sprite for digit Vx.
                        // The value of I is set to the location for the hexadecimal sprite
                        // corresponding to the value of Vx. See section 2.4, Display, for more
                        // information on the Chip-8 hexadecimal font.
                        debug!("0x{:X}: Set idx =  location of sprite for digit Vx", opcode);
                        self.idx = (self.v[x].0 * 5) as u16;
                    },
                    0x33 => { //Store BCD representation of Vx in memory locations I, I+1, and I+2.
                        // The interpreter takes the decimal value of Vx, and places the hundreds
                        // digit in memory at location in I, the tens digit at location I+1, and the
                        // ones digit at location I+2.
                        debug!("0x{:X}: Set idx + (0,1,2) =  BCD(Vx)", opcode);
                        let bcd = ChipEight::byte_to_bcd(self.v[x].0);

                        for i in 0..3 {
                           match bcd.get(i) {
                               Some(digit) => self.memory[(self.idx + (i as u16)) as usize] = digit.clone(),
                               None => println!("bcd error") //todo error
                           }
                        }

                    },
                    0x55 => { // Store registers V0 through Vx in memory starting at location I.
                        // The interpreter copies the values of registers V0 through Vx into memory,
                        // starting at the address in I.
                        debug!("0x{:X}: Store V0:Vx in memory starting at location I", opcode);
                        for i in 0..=x {
                            self.memory[(self.idx + (i as u16)) as usize] = self.v[i].0;
                        }
                    },
                    0x65 => { // Read registers V0 through Vx from memory starting at location I.
                        // The interpreter reads values from memory starting at location I into
                        // registers V0 through Vx.
                        debug!("0x{:X}: Read V0:Vx into memory starting at location I", opcode);
                        for i in 0..=x {
                            self.v[i] = Wrapping(self.memory[(self.idx + (i as u16)) as usize]);
                        }
                    }
                    _ => {
                        error!("Unknown opcode: 0x{:X}", opcode);
                    }
                }
            }


            _ => {
                error!("Unknown opcode: 0x{:X}", opcode);
            }
        }

        if inc_pc {
            self.pc += 2;
        }
    }

    fn byte_to_bcd(byte: u8) -> Vec<u8> {
        let mut bcd: Vec<u8> = vec![];

        let mut byte = byte;

        while byte > 0 {
            bcd.push(byte  % 10);
            byte /= 10;
        }

        // Ensure we go to the hundredths place at least
        if bcd.len() < 3 {
            for _ in 0..(3 - bcd.len()) {
                bcd.push(0)
            }
        }

        bcd.reverse();
        bcd
    }

    fn update_timers(&mut self) {
        if self.delay_time > 0 {
            self.delay_time -= 1;
        }

        if self.sound_timer > 0 {
            println!("BEEP");
            self.sound_timer -= 1;
        }
    }


}

#[cfg(test)]
mod tests {
    use crate::chip8::ChipEight;
    use crate::chip8::FONT_SET;
    use std::num::Wrapping;

    #[test]
    fn chip8_inits_correctly() {
        let chip8 = ChipEight::init();

        assert_eq!(0x200, chip8.pc);

        for i in 0..80 {
            assert_eq!(FONT_SET[i], chip8.memory[i])
        }
    }


    #[test]
    fn chip8_fetch_opcode_correctly() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x12;
        chip8.memory[(chip8.pc as usize) + 1] = 0x34;

        let opcode = chip8.fetch_opcode();

        assert_eq!(0x1234, opcode);
    }

    #[test]
    fn chip8_0x00E0_clears_display_correctly() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x00;
        chip8.memory[(chip8.pc as usize) + 1] = 0xE0;

        for i in 0..chip8.gfx.len() {
            chip8.gfx[i] = 1;
        }

        chip8.cycle();

        for i in 0..chip8.gfx.len() {
            assert_eq!(0,chip8.gfx[i]);
        }
    }

    #[test]
    fn chip8_0x00EE_returns_correctly() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x00;
        chip8.memory[(chip8.pc as usize) + 1] = 0xEE;

        chip8.stack.push(0xBEEF);

        chip8.cycle();

        assert_eq!(0xBEEF + 2, chip8.pc);
        assert_eq!(0, chip8.stack.len());
    }

    #[test]
    fn chip8_0x1nnn_jumps_to_location_correctly() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x12;
        chip8.memory[(chip8.pc as usize) + 1] = 0x62;

        chip8.cycle();

        assert_eq!(0x262, chip8.pc);
    }

    #[test]
    fn chip8_0x2nnn_call_addr_correctly() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x23;
        chip8.memory[(chip8.pc as usize) + 1] = 0x45;

        chip8.cycle();

        assert_eq!(0x345, chip8.pc);
        assert_eq!(0x200, chip8.stack.pop().unwrap());
        assert_eq!(0, chip8.stack.len());
    }

    #[test]
    fn chip8_0x3xkk_skip_when_equal() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x30;
        chip8.memory[(chip8.pc as usize) + 1] = 0x45;

        chip8.v[0] = Wrapping(0x45);

        chip8.cycle();

        assert_eq!(0x200 + 4, chip8.pc);
    }

    #[test]
    fn chip8_0x3xkk_dont_skip_when_not_equal() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x30;
        chip8.memory[(chip8.pc as usize) + 1] = 0x45;

        chip8.v[0] = Wrapping(0x44);

        chip8.cycle();

        assert_eq!(0x200 + 2, chip8.pc);
    }

    #[test]
    fn chip8_0x4xkk_skip_when_not_equal() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x40;
        chip8.memory[(chip8.pc as usize) + 1] = 0x45;

        chip8.v[0] = Wrapping(0x44);

        chip8.cycle();

        assert_eq!(0x200 + 4, chip8.pc);
    }

    #[test]
    fn chip8_0x4xkk_dont_skip_when_equal() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x40;
        chip8.memory[(chip8.pc as usize) + 1] = 0x45;

        chip8.v[0] = Wrapping(0x45);

        chip8.cycle();

        assert_eq!(0x200 + 2, chip8.pc);
    }

    #[test]
    fn chip8_0x5xy0_skip_when_vx_eq_vy() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x50;
        chip8.memory[(chip8.pc as usize) + 1] = 0x10;

        chip8.v[0] = Wrapping(0x45);
        chip8.v[1] = Wrapping(0x45);

        chip8.cycle();

        assert_eq!(0x200 + 4, chip8.pc);
    }

    #[test]
    fn chip8_0x5xy0_dont_skip_when_vx_ne_vy() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x50;
        chip8.memory[(chip8.pc as usize) + 1] = 0x10;

        chip8.v[0] = Wrapping(0x45);
        chip8.v[1] = Wrapping(0x44);

        chip8.cycle();

        assert_eq!(0x200 + 2, chip8.pc);
    }

    #[test]
    fn chip8_0x6xkk_load_vx_correctly() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x60;
        chip8.memory[(chip8.pc as usize) + 1] = 0x45;

        chip8.cycle();

        assert_eq!(0x45, chip8.v[0].0);
    }

    #[test]
    fn chip8_0x7xkk_add_kk_to_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x70;
        chip8.memory[(chip8.pc as usize) + 1] = 0x45;

        chip8.v[0] = Wrapping(1);

        chip8.cycle();

        assert_eq!(0x46, chip8.v[0].0);
    }

    #[test]
    fn chip8_0x8xy0_load_vy_to_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x80;
        chip8.memory[(chip8.pc as usize) + 1] = 0x10;

        chip8.v[1] = Wrapping(0x45);

        chip8.cycle();

        assert_eq!(0x45, chip8.v[0].0);
    }

    #[test]
    fn chip8_0x8xy1_or_vy_vx_into_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x80;
        chip8.memory[(chip8.pc as usize) + 1] = 0x11;

        chip8.v[0] = Wrapping(1);
        chip8.v[1] = Wrapping(1);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 1);


        chip8.pc = 0x200;
        chip8.v[0] = Wrapping(8);
        chip8.v[1] = Wrapping(16);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 24);
    }

    #[test]
    fn chip8_0x8xy2_and_vy_vx_into_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x80;
        chip8.memory[(chip8.pc as usize) + 1] = 0x12;

        chip8.v[0] = Wrapping(1);
        chip8.v[1] = Wrapping(1);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 1);


        chip8.pc = 0x200;
        chip8.v[0] = Wrapping(8);
        chip8.v[1] = Wrapping(16);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 0);
    }

    #[test]
    fn chip8_0x8xy3_xor_vy_vx_into_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x80;
        chip8.memory[(chip8.pc as usize) + 1] = 0x13;

        chip8.v[0] = Wrapping(1);
        chip8.v[1] = Wrapping(2);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 3);


        chip8.pc = 0x200;
        chip8.v[0] = Wrapping(4);
        chip8.v[1] = Wrapping(5);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 1);
    }

    #[test]
    fn chip8_0x8xy4_add_vy_vx_into_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x80;
        chip8.memory[(chip8.pc as usize) + 1] = 0x14;

        chip8.v[0] = Wrapping(1);
        chip8.v[1] = Wrapping(2);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 3);


        chip8.pc = 0x200;
        chip8.v[0] = Wrapping(4);
        chip8.v[1] = Wrapping(5);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 9);
    }

    #[test]
    fn chip8_0x8xy4_add_vy_vx_into_vx_with_carry() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x80;
        chip8.memory[(chip8.pc as usize) + 1] = 0x14;

        chip8.v[0] = Wrapping(255);
        chip8.v[1] = Wrapping(1);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 0);
    }

    #[test]
    fn chip8_0x8xy5_sub_vy_vx_into_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x80;
        chip8.memory[(chip8.pc as usize) + 1] = 0x15;

        chip8.v[0] = Wrapping(4);
        chip8.v[1] = Wrapping(2);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 2);


        chip8.pc = 0x200;
        chip8.v[0] = Wrapping(10);
        chip8.v[1] = Wrapping(5);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 5);
    }

    #[test]
    fn chip8_0x8xy5_sub_vy_vx_into_vx_with_borrow() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x80;
        chip8.memory[(chip8.pc as usize) + 1] = 0x15;

        chip8.v[0] = Wrapping(1);
        chip8.v[1] = Wrapping(2);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 255);
    }

    #[test]
    fn chip8_0x8xy6_shr_vx_1_into_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x80;
        chip8.memory[(chip8.pc as usize) + 1] = 0x16;

        chip8.v[0] = Wrapping(5);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 2);
        assert_eq!(chip8.v[0xF].0, 1);


        chip8.pc = 0x200;
        chip8.v[0] = Wrapping(8);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 4);
        assert_eq!(chip8.v[0xF].0, 0);
    }

    #[test]
    fn chip8_0x8xy7_subn_vy_vx_into_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x80;
        chip8.memory[(chip8.pc as usize) + 1] = 0x17;

        chip8.v[0] = Wrapping(2);
        chip8.v[1] = Wrapping(3);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 1);
        assert_eq!(chip8.v[0xF].0, 1);


        chip8.pc = 0x200;
        chip8.v[0] = Wrapping(16);
        chip8.v[1] = Wrapping(24);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 8);
        assert_eq!(chip8.v[0xF].0, 1);
    }

    #[test]
    fn chip8_0x8xy7_subn_vy_vx_into_vx_with_borrow() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x80;
        chip8.memory[(chip8.pc as usize) + 1] = 0x17;

        chip8.v[0] = Wrapping(37);
        chip8.v[1] = Wrapping(25);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 244);
        assert_eq!(chip8.v[0xF].0, 0);


        chip8.pc = 0x200;
        chip8.v[0] = Wrapping(16);
        chip8.v[1] = Wrapping(8);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 248);
        assert_eq!(chip8.v[0xF].0, 0);
    }

    #[test]
    fn chip8_0x8xy6_shl_vx_1_into_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x80;
        chip8.memory[(chip8.pc as usize) + 1] = 0x1E;

        chip8.v[0] = Wrapping(5);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 10);
        assert_eq!(chip8.v[0xF].0, 0);


        chip8.pc = 0x200;
        chip8.v[0] = Wrapping(128);

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 0);
        assert_eq!(chip8.v[0xF].0, 1);
    }

    #[test]
    fn chip8_0x9xy0_sne_vx_ne_vy() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x90;
        chip8.memory[(chip8.pc as usize) + 1] = 0x10;

        chip8.v[0] = Wrapping(4);
        chip8.v[1] = Wrapping(5);

        chip8.cycle();

        assert_eq!(chip8.pc, 0x200 + 4);
    }

    #[test]
    fn chip8_0x9xy0_dont_sne_vx_eq_vy() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0x90;
        chip8.memory[(chip8.pc as usize) + 1] = 0x10;

        chip8.v[0] = Wrapping(5);
        chip8.v[1] = Wrapping(5);

        chip8.cycle();

        assert_eq!(chip8.pc, 0x200 + 2);
    }

    #[test]
    fn chip8_0xAnnn_ld_pc_with_nnn() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0xA1;
        chip8.memory[(chip8.pc as usize) + 1] = 0x23;

        chip8.cycle();

        assert_eq!(chip8.idx, 0x123);
    }

    #[test]
    fn chip8_0xBnnn_JP_v0_plus_nnn() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0xB2;
        chip8.memory[(chip8.pc as usize) + 1] = 0x33;

        chip8.v[0] = Wrapping(1);
        chip8.cycle();

        assert_eq!(chip8.pc, 0x234);
    }

    #[test]
    fn chip8_0xFx07_set_vx_to_delay_time() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0xF0;
        chip8.memory[(chip8.pc as usize) + 1] = 0x07;

        chip8.delay_time = 5;

        chip8.cycle();

        assert_eq!(chip8.v[0].0, 5);

    }

    #[test]
    fn chip8_0xFx15_set_delay_timer_to_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0xF0;
        chip8.memory[(chip8.pc as usize) + 1] = 0x15;

        chip8.v[0] = Wrapping(3);

        chip8.cycle();

        assert_eq!(chip8.delay_time, 2);
    }

    #[test]
    fn chip8_0xFx18_set_sound_timer_to_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0xF0;
        chip8.memory[(chip8.pc as usize) + 1] = 0x18;

        chip8.v[0] = Wrapping(3);

        chip8.cycle();

        assert_eq!(chip8.sound_timer, 2);
    }

    #[test]
    fn chip8_0xFx1E_set_idx_to_idx_plus_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0xF0;
        chip8.memory[(chip8.pc as usize) + 1] = 0x1E;

        chip8.idx = 5;
        chip8.v[0] = Wrapping(3);

        chip8.cycle();

        assert_eq!(chip8.idx, 8);
    }

    #[test]
    fn chip8_0xFx29_set_idx_to_location_of_sprite_vx() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0xF0;
        chip8.memory[(chip8.pc as usize) + 1] = 0x29;

        chip8.v[0] = Wrapping(3);

        chip8.cycle();

        assert_eq!(chip8.idx, chip8.v[0].0 as u16 * 5);
    }

    #[test]
    fn chip8_0xFx33_store_bcd_of_vx_in_memory_idx012() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0xF0;
        chip8.memory[(chip8.pc as usize) + 1] = 0x33;

        chip8.v[0] = Wrapping(123);

        chip8.cycle();

        assert_eq!(chip8.memory[(chip8.idx as usize)], 1);
        assert_eq!(chip8.memory[(chip8.idx + 1) as usize], 2);
        assert_eq!(chip8.memory[(chip8.idx + 2) as usize], 3);
    }

    #[test]
    fn chip8_0xFx55_store_v0_vx_in_memory() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0xF5;
        chip8.memory[(chip8.pc as usize) + 1] = 0x55;


        for i in 0..6 {
            chip8.v[i] = Wrapping(i as u8);
        }
        chip8.idx = 300;

        chip8.cycle();

        for i in 0..6 {
            assert_eq!(chip8.memory[(chip8.idx + i) as usize], i as u8);
        }
    }

    #[test]
    fn chip8_0xFx55_read_v0_vx_from_memory() {
        let mut chip8 = ChipEight::init();
        chip8.memory[chip8.pc as usize] = 0xF5;
        chip8.memory[(chip8.pc as usize) + 1] = 0x65;


        chip8.idx = 300;
        for i in 0..6 {
            chip8.memory[(chip8.idx + i) as usize] = i as u8;
        }

        chip8.cycle();

        for i in 0..6 {
            assert_eq!(chip8.v[i as usize].0, i as u8);
        }
    }
}