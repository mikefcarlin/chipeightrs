pub mod chip8;
pub mod gameloop;
#[macro_use]
extern crate crossbeam_channel;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
