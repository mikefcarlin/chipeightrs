extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;

extern crate clap;
use clap::{Arg};

use piston::window::WindowSettings;
use piston::event_loop::*;
use piston::input::*;
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{ GlGraphics, OpenGL };
use std::fs::File;
use std::io::Read;
use std::process::exit;
use chip8rs::gameloop::{GameLoop, GameEvent};
use crossbeam_channel::unbounded;
use log::{info, trace, warn, debug};

pub struct App {
    gl: GlGraphics, // OpenGL drawing backend.
    pixels: Vec<u8>,
}

impl App {
    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 0.0];
        const WHITE:   [f32; 4] = [1.0, 1.0, 1.0, 1.0];

        let (x_size, y_size) = (args.window_size[0],
                      args.window_size[1]);

        let pixel_size = (x_size/64.0, y_size/32.0);

        let pc = self.pixels.clone();
        self.gl.draw(args.viewport(), |c, gl, | {
            // Clear the screen.
            clear(BLACK, gl);

            for (i, pixel) in pc.iter().enumerate() {
                let y_row = i / 64;
                let x_row = i % 64;
                if pixel != &0_u8 {
                    let x0 = (x_row as f64) * pixel_size.0;
                    let y0 = (y_row as f64) * pixel_size.1;
                    let x1 = x0 + pixel_size.0;
                    let y1 = y0 + pixel_size.1;
                    rectangle(WHITE, rectangle::rectangle_by_corners(x0, y0, x1, y1), c.transform, gl)
                }
            }

        });
    }

    fn update(&mut self, _args: &UpdateArgs) {

    }
}

fn main() {
    env_logger::init();

    let matches = clap::App::new("Chip8 Emulator")
        .version("1.0")
        .about("Runs Chip8 Games via an emulator")
        .author("Mike Carlin")
        .arg(Arg::with_name("game")
            .short("g")
            .long("game")
            .value_name("FILE")
            .help("Sets the game file to load")
            .required(true)
            .takes_value(true))
        .get_matches();

    let game_path = matches.value_of("game").unwrap();
    let mut game_file = match File::open(game_path) {
        Ok(f) => f,
        Err(e) => {
            eprintln!("{}", e);
            exit(1);
        }
    };

    let mut game_bytes = vec!();
    let _size = match game_file.read_to_end(&mut game_bytes) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("{}", e);
            exit(2);
        }
    };


    // Change this to OpenGL::V2_1 if not working.
    let opengl = OpenGL::V3_2;

    // Create an Glutin window.
    let mut window: Window = WindowSettings::new(
        "Chip8 Emulator",
        [512, 256]
    )
        .graphics_api(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    // Create a new game and run it.
    let mut app = App {
        gl: GlGraphics::new(opengl),
        pixels: vec![]
    };


    let (s, r) = unbounded();
    let game_loop = GameLoop::init("game".to_string(), game_bytes, s);
    game_loop.start();

    let mut events = Events::new(EventSettings::new());

    loop {
        if let Some(event) = events.next(&mut window) {
            if let Some(r) = event.render_args() {
                app.render(&r);
            }

            if let Some(u) = event.update_args() {
                app.update(&u);
            }

            if let Some(Button::Keyboard(key)) = event.press_args() {
               match key {
                   Key::D1 => game_loop.key_press(1),
                   Key::D2 => game_loop.key_press(2),
                   Key::D3 => game_loop.key_press(3),
                   Key::D4 => game_loop.key_press(0xC),

                   Key::Q  => game_loop.key_press(4),
                   Key::W => game_loop.key_press(5),
                   Key::E => game_loop.key_press(6),
                   Key::R => game_loop.key_press(0xD),

                   Key::A  => game_loop.key_press(7),
                   Key::S => game_loop.key_press(8),
                   Key::D => game_loop.key_press(9),
                   Key::F => game_loop.key_press(0xE),

                   Key::Z  => game_loop.key_press(0xA),
                   Key::X => game_loop.key_press(0),
                   Key::C => game_loop.key_press(0xB),
                   Key::V => game_loop.key_press(0xF),

                   _ => {}
               }
            };

            if let Some(button) = event.release_args() {
                match button {
                    Button::Keyboard(key) => {
                        match key {
                            Key::D1 => game_loop.key_release(1),
                            Key::D2 => game_loop.key_release(2),
                            Key::D3 => game_loop.key_release(3),
                            Key::D4 => game_loop.key_release(0xC),

                            Key::Q => game_loop.key_release(4),
                            Key::W => game_loop.key_release(5),
                            Key::E => game_loop.key_release(6),
                            Key::R => game_loop.key_release(0xD),

                            Key::A => game_loop.key_release(7),
                            Key::S => game_loop.key_release(8),
                            Key::D => game_loop.key_release(9),
                            Key::F => game_loop.key_release(0xE),

                            Key::Z => game_loop.key_release(0xA),
                            Key::X => game_loop.key_release(0),
                            Key::C => game_loop.key_release(0xB),
                            Key::V => game_loop.key_release(0xF),

                            _ => {}
                        }
                    },
                    _ => {}
                }
            };
        }
        for game_event in r.try_iter() {
            match game_event {
                GameEvent::UpdateDraw(p) => {
                    debug!("Update Draw event");
                    app.pixels.clone_from(&p);
                }
            }
        }
    }
}

